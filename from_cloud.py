from transformers import AutoTokenizer, TFAutoModelWithLMHead

# Download and export tokenizer
tokenizer = AutoTokenizer.from_pretrained("gpt2-large")
tokenizer.save_pretrained("projects/sample")

# Download GPT-2 345M
# model = TFAutoModelWithLMHead.from_pretrained("gpt2-large")
